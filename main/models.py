# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.

from django.utils import timezone                   #for default=timezone.now
from .validators import *                           #for image=
from django.utils.safestring import mark_safe       #for imagePreView

###################################################################################################################
################################################# Abstract Class___b ##############################################
###################################################################################################################
class abstractClassD(models.Model):
    dateCreate = models.DateField(blank=False, default=timezone.now, verbose_name="Дата")#, auto_now_add=True)

    class Meta:
        abstract = True
        ordering = ["-dateCreate"]

class abstractClassDId(abstractClassD):
    id_fk       = models.ForeignKey('mainLanding', on_delete=models.CASCADE)

    class Meta(abstractClassD.Meta):
        abstract = True
###################################################################################################################
################################################# Abstract Class___e ##############################################
###################################################################################################################
################################################### MAIN_CLASS___b ################################################
###################################################################################################################
class mainLanding(abstractClassD):
    thisIsLandActive    = models.BooleanField(default=False, verbose_name="Активен")
    nameLanding         = models.CharField(blank=False,  default="", max_length=100, verbose_name="Название Лендинга")

    mainBlockSiteActive     = models.BooleanField(default=False, verbose_name="Закрыть сайт по:")
    mainBlockSiteChoice     = models.ForeignKey('mainBlockSite', on_delete=models.CASCADE, blank=True, null=True, verbose_name="Выберите причину не работоспособности ресурса")

    mainTopChoice           = models.ForeignKey('mainTop', on_delete=models.CASCADE, blank=True, null=True, verbose_name="Выберите Топ из списка:")

    mainSpecificationChoice = models.ForeignKey('mainSpecification', on_delete=models.CASCADE, blank=True, null=True, verbose_name="Выберите Блок Спецификации из списка:")

    # mainBlockSiteActive.short_description = "Children"
    def __str__(self):
        return self.nameLanding

    class Meta(abstractClassD.Meta):
        verbose_name = "Главная (Лендинг)"
        verbose_name_plural = "Главные (Лендинги)"
###################################################################################################################
################################################### MAIN_CLASS___e ################################################
###################################################################################################################

class mainBlockSite(abstractClassDId):
    siteBlockTextTitle  = models.CharField(blank=True,  default="", max_length=100, verbose_name="Титул Когда Сайт ЗАБЛОКИРОВАН")
    siteBlockText       = models.TextField(blank=True,  default="", max_length=500, verbose_name="Текст Когда Сайт ЗАБЛОКИРОВАН")

    def __str__(self):
        return self.siteBlockTextTitle

    class Meta(abstractClassDId.Meta):
        verbose_name = "Текст Блокировки Сайта"
        verbose_name_plural = "Тексты Блокировки Сайта"

class mainTop(abstractClassDId):
    mainTitle   = models.CharField(blank=True,  default="", max_length=100, verbose_name="Титул")
    mainText    = models.TextField(blank=True,  default="", max_length=500, verbose_name="Текст")

    def __str__(self):
        return self.mainTitle

    class Meta(abstractClassDId.Meta):
        verbose_name = "Топ-Хэдер"
        verbose_name_plural = "Топы-Хэдеры"


class mainSpecification(abstractClassDId):
    title = models.CharField(blank=True,  default="", max_length=101, verbose_name="Титул")

    def __str__(self):
        return self.title

    class Meta(abstractClassDId.Meta):
        verbose_name = "Спецификация"
        verbose_name_plural = "Спецификации"

    # def get_edit_link(self):
    #     return "ewefff3f34f34"

class mainSpecificationItem(abstractClassD):
    id_fk   = models.ForeignKey(mainSpecification, on_delete=models.CASCADE)
    title   = models.CharField(blank=True,  default="", max_length=100, verbose_name="Титул")
    text    = models.TextField(blank=True,  default="", max_length=500, verbose_name="Текст")
    image   = models.FileField(default='', validators=[validate_file_extension_picture], verbose_name="Картинка:", upload_to='main/specification/')

    def imagePreView(self):
        return mark_safe('<img src="/media/%s" height="150" />' % (self.image))
        # return mark_safe('<img src="/media/%s" height="150" />' % (self.image))
    imagePreView.short_description = 'Предпросмотр картинки'

    def __str__(self):
        return self.title

    class Meta(abstractClassD.Meta):
        verbose_name = "Спецификация Элемент"
        verbose_name_plural = "Спецификации Элементы"

###################################################################################################################
###################################################################################################################
# import os
# from django.utils.text import slugify
# def upload_to(instance, filename):
#     filename_base, filename_ext = os.path.splitext(filename)
#     return "painters/{painter}/{filename}{extension}".format(
#         painter=slugify(instance.painter.name),
#         filename=slugify(filename_base),
#         extension=filename_ext.lower(),
#     )