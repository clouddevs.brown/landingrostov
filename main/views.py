# -*- coding: utf-8 -*-

from django.shortcuts import render

# Create your views here.
from django.views import View
from .models import *

class mainLandingView(View):
    # model = mainLanding
    template_name = 'main.html'

    def get(self, request):#, *args, **kwargs):
        context = dict()
        return render(request, self.template_name, context)
