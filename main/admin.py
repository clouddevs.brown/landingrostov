# -*- coding: utf-8 -*-

from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import *
from tabbed_admin import TabbedModelAdmin
from django.urls import reverse
from django.utils.safestring import mark_safe

# import nested_admin


class mainBlockSiteInline(admin.StackedInline):
    model = mainBlockSite
    readonly_fields = ['dateCreate', ]
    extra = 1

class mainTopInline(admin.StackedInline):
    model = mainTop
    readonly_fields = ['dateCreate', ]
    extra = 1

class mainSpecificationItemInline(admin.StackedInline):
    model = mainSpecificationItem
    readonly_fields = ['dateCreate', 'imagePreView']
    extra = 1

@admin.register(mainSpecification)
class mainSpecificationAdmin(admin.ModelAdmin):
    save_on_top = True
    fields = ['title', 'dateCreate', ]
    # readonly_fields = [get_picture_preview]
    inlines = [mainSpecificationItemInline]

    def has_add_permission(self, request):
        return False
    def has_change_permission(self, request, obj=None):
        return False
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        # perms = admin.ModelAdmin.get_model_perms(self, *args, **kwargs)
        # perms['list_hide'] = True
        # return perms
        return {}

class mainSpecificationInline(admin.StackedInline):
    model = mainSpecification
    fields = ["get_edit_link", 'title', 'dateCreate',]
    readonly_fields = ['dateCreate', 'get_edit_link']
    extra = 1

    def get_edit_link(self, obj):
        if obj.pk:  # if object has already been saved and has a primary key, show link to it
            url = reverse( 'admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name), args=[obj.pk,])
            return mark_safe('<b><a style="color: #090;" href="{url}">{text}</a></b>'.format(
                url=url,
                text=('Кликните по Этой ссылке для редактирования блока "%s" ("%s")') % (obj.title, obj._meta.verbose_name)
            ))
        return mark_safe('<b style="color: #900;"> Создайте элемент и отредактируйте его после!!</b>')
    get_edit_link.short_description = "Редактировать блок"


@admin.register(mainLanding)
class mainLandingAdmin(TabbedModelAdmin):
    save_on_top = True
    model               = mainLanding
    readonly_fields     = ['dateCreate', ]
    list_display        = ['thisIsLandActive', 'nameLanding', 'dateCreate', 'mainBlockSiteActive', ]
    list_display_links  = ['nameLanding',]

    tab_mainLandingInline = (
        (None, {
            'fields': ('nameLanding', 'dateCreate',)
        }),
        ('БЛОКИРОВКА САЙТА!!', {
            'fields': ('mainBlockSiteActive', 'mainBlockSiteChoice', )
        }),
        mainBlockSiteInline,
    )
    tab_mainTopInline = (
        ('Выбор Топа:', {
            'fields': ('mainTopChoice',)
        }),
        mainTopInline,
    )
    tab_mainSpecificationInline = (
        ('Выбор Спецификации:', {
            'fields': ('mainSpecificationChoice',)
        }),
        mainSpecificationInline,
    )
    tabs = [
        ('Основные', tab_mainLandingInline),
        ('Топ', tab_mainTopInline),
        ('Cпецификации', tab_mainSpecificationInline),
    ]

# admin.site.register(mainLanding, mainLandingAdmin)






# class mainSpecificationItemInline(nested_admin.NestedStackedInline):
#     model = mainSpecificationItem
#     readonly_fields = ['dateCreate', ]
#     extra = 1
#
# class mainSpecificationInline(nested_admin.NestedStackedInline):
#     model = mainSpecification
#     readonly_fields = ['dateCreate', ]
#     extra = 1
#     inlines = [
#         mainSpecificationItemInline,
#     ]
# class wde(nested_admin.NestedModelAdmin):
#     model = mainLanding
#     inlines = [
#         mainSpecificationInline,
#     ]
# admin.site.register(mainLanding, wde)