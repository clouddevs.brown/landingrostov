# Generated by Django 2.2.2 on 2019-06-28 10:28

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='mainBlockSite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dateCreate', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('siteBlockTextTitle', models.CharField(blank=True, default='', max_length=100, verbose_name='Титул Когда Сайт ЗАБЛОКИРОВАН')),
                ('siteBlockText', models.TextField(blank=True, default='', max_length=500, verbose_name='Текст Когда Сайт ЗАБЛОКИРОВАН')),
            ],
            options={
                'verbose_name': 'Текст Блокировки Сайта',
                'verbose_name_plural': 'Тексты Блокировки Сайта',
                'ordering': ['-dateCreate'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='mainLanding',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dateCreate', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('thisIsLandActive', models.BooleanField(default=False, verbose_name='Активен')),
                ('nameLanding', models.CharField(default='', max_length=100, verbose_name='Название Лендинга')),
                ('mainBlockSiteActive', models.BooleanField(default=False, verbose_name='Закрыть сайт по:')),
                ('mainBlockSiteChoice', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.mainBlockSite', verbose_name='Выберите причину не работоспособности ресурса')),
            ],
            options={
                'verbose_name': 'Главная (Лендинг)',
                'verbose_name_plural': 'Главные (Лендинги)',
                'ordering': ['-dateCreate'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='rds',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dateCreate', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('title', models.CharField(blank=True, default='', max_length=101, verbose_name='Титул')),
                ('id_fk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.mainLanding')),
            ],
        ),
        migrations.CreateModel(
            name='mainTop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dateCreate', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('mainTitle', models.CharField(blank=True, default='', max_length=100, verbose_name='Титул')),
                ('mainText', models.TextField(blank=True, default='', max_length=500, verbose_name='Текст')),
                ('id_fk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.mainLanding')),
            ],
            options={
                'verbose_name': 'Топ-Хэдер',
                'verbose_name_plural': 'Топы-Хэдеры',
                'ordering': ['-dateCreate'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='mainlanding',
            name='mainTopChoice',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.mainTop', verbose_name='Выберите Топ из списка:'),
        ),
        migrations.AddField(
            model_name='mainblocksite',
            name='id_fk',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.mainLanding'),
        ),
    ]
