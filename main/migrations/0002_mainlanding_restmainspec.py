# Generated by Django 2.2.2 on 2019-06-28 10:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainlanding',
            name='restmainSpec',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='main.rds'),
            preserve_default=False,
        ),
    ]
